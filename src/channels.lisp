;;;; channels.lisp: logic to compute channel participation.
;;; This file is a part of cl-decentralise.

(in-package :decentralise)

(defun should-send (subscribed-channels block-channels)
  (or (member "all" subscribed-channels :test #'equal)
      (dolist (channel block-channels)
        (when (member channel subscribed-channels :test #'equal)
          (return t)))))

(defun maybe-send-to-meta (meta block-name block-data block-version block-channels)
  (when (should-send (connection-meta-channels meta)
                     block-channels)
    (write-block (connection-meta-socket meta)
                 block-name
                 block-data
                 :version block-version
                 :channels block-channels)))

(defun send-to-all-interested (system meta block-name)
  (with-slots (get-item connection-list) system
    (multiple-value-bind (block-data block-version block-channels)
        (get-item system block-name)
      (dolist (connection connection-list)
        (unless (eq meta connection)
          (maybe-send-to-meta connection block-name
                              block-data block-version
                              block-channels))))))
