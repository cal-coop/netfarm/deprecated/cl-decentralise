;;;; network.lisp: helper code for reading and writing blocks and text
;;; This file is a part of cl-decentralised.

(in-package :decentralise)

;;; All this is specific to the cl-decentralise protocol.

(defvar +eof-marker+ "--EOF--")

(defun split-by-spaces (line)
  (split-sequence #\Space line))

(defun connect-to-ip (ip &optional (port 1892))
  (let ((connection (make-instance 'connection :ip ip :port port)))
    (setf (connection-conn connection)
          (usocket:socket-stream
           (usocket:socket-connect ip port)))
    connection))

(declaim (inline read-conn-line))

(defun read-conn-line (connection)
  (read-line (connection-conn connection)))

(defun read-status-line (connection)
  (split-by-spaces
   (string-downcase
    (read-conn-line connection))))

(defun read-block (connection)
  (let ((status-line (read-status-line connection)))
    (if (string/= (first status-line) "block")
        ;; Errors don't have data.
        (error (format nil "~{~a~^ ~}" status-line))
        (values (read-rest-of-block connection)
                (second status-line)))))

(defun read-rest-of-block (connection)
  (with-output-to-string (output-stream)
    (loop
       for line = (read-conn-line connection)
       for first = nil then t
       until (string= line "--EOF--")
       when first
       do (terpri output-stream)
       do (write-string line output-stream))))

(defun write-text (connection text)
  (princ text (connection-conn connection))
  (terpri (connection-conn connection))
  (finish-output (connection-conn connection)))

(defun stringify-address (name)
  (if (stringp name) name
      (let ((lname (coerce name 'list)))
        (ecase (length lname) ; IPv4 or 6?
          (4 (format nil "~{~d~^.~}" lname))
          (6 (format nil "~{~d~^:~}" lname))))))

;;; This is somewhat internal, but could be applied to other protocols.

(defmethod bind ((system system) &key (host "0.0.0.0"))
  (setf (slot-value system 'connection)
        (socket-listen host
                       (slot-value system 'port)
                       :reuse-address t)))
(defmethod protocol-name ((connection connection)) "netfarmprot")
(defmethod connection-name ((connection connection))
  (format nil "~a:~a:~d"
          (protocol-name connection)
          (connection-ip connection)
          (connection-port connection)))

(setf (gethash "netfarmprot" *known-protocols*)
      (lambda (ip port timeout)
        (let ((socket (socket-connect ip port
                                      :timeout timeout
                                      :nodelay t)))
          (make-instance 'connection
                         :ip ip
                         :port port
                         :connection (socket-stream socket)
                         :socket socket))))

(defmethod close-connection ((connection connection))
  (close (connection-conn connection))
  (when (slot-boundp connection 'raw-socket)
    (socket-close (connection-socket connection))))

