;;;; client.lisp: operations on the decentralised protocol.
;;; This file is a part of cl-decentralise.

(in-package :decentralise)

(defmethod write-block ((connection connection) name values &key
                                                              (version 0)
                                                              (channels nil))
  "Send a new block."
  (format (connection-conn connection)
          "BLOCK ~a ~d~{ ~a~}~%"
          name
          version
          channels)
  (etypecase values
    (string (write-string values
                          (connection-conn connection))
            (terpri (connection-conn connection)))
    (list
     (dolist (line values)
       (write-line line (connection-conn connection)))))
  (write-line +eof-marker+ (connection-conn connection))
  (finish-output (connection-conn connection)))

(defmethod get-blocks ((connection connection) names)
  (format (connection-conn connection)
          "GET ~{~a~^ ~}~%"
          names)
  (finish-output (connection-conn connection)))

(defmethod write-error ((connection connection) name &optional (reason ""))
  (format (connection-conn connection)
          "ERROR ~a ~a~%" name reason)
  (finish-output (connection-conn connection)))

(defmethod write-ok ((connection connection) name)
  (format (connection-conn connection)
          "OK ~a~%" name)
  (finish-output (connection-conn connection)))

(defmethod subscribe-channels ((connection connection) channels)
  (format (connection-conn connection)
          "SUBSCRIBE ~{~a~^ ~}~%" channels)
  (finish-output (connection-conn connection)))

(defmethod announce ((connection connection) &optional (announcep t))
  (format (connection-conn connection)
          "ANNOUNCE ~a~%"
          (if announcep "yes" "no"))
  (finish-output (connection-conn connection)))

(defmethod read-event ((connection connection))
  (let ((status (read-status-line connection)))
    (macrolet ((possible-event-types (&body types)
                 `(cond
                    ,@(loop for type in types collect
                           (destructuring-bind (name binds &body body) type
                               `((string= (first status) ,name)
                                 (destructuring-bind ,binds (rest status) ,@body)))))))
      (possible-event-types
       ("get" (&rest names) `(:get ,names))
       ("block" (name version-string &rest channels)
                `(:block ,name ,(parse-integer version-string)
                         ,channels
                         ,(read-rest-of-block connection)))
       ("ok" (name) `(:ok ,name))
       ("error" (name &rest error-bits)
                `(:error ,name ,(apply #'concatenate 'string error-bits)))
       ("subscribe" (&rest channels)
                    `(:subscribe ,channels))
       ("announce" (string)
                   `(:announce ,(string= string "yes")))))))
