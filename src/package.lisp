;;;; package.lisp: the DEFPACKAGE.
;;; This file is a part of cl-decentralise.

(defpackage :decentralise
  (:use :cl :usocket :split-sequence :bordeaux-threads :optima)
  (:export #:system
           ;; event handlers
           #:with-event-handlers
           ;; sync
           #:bind
           #:accept
           #:connect-to-ip
           ;; network
           #:connection
           #:connection-ip
           #:connection-port
           #:connection-conn
           #:*known-protocols*
           #:make-connection-from-uri
           ;; client
           #:write-block
           #:get-block
           #:get-blocks
           #:write-error
           #:write-ok
           #:subscribe-channels
           #:announce
           ;; protocol
           #:read-event
           #:write-error
           #:write-ok
           #:write-block
           #:subscribe-channels
           #:announce
           #:close-connection
           ;; accessors
           #:get-item
           #:put-item
           #:listing-generator
           #:stop-put-p
           #:system-connection-list
           #:system-sync-limit
           ;; readers
           #:system-port))
