(defsystem decentralise-example
  :depends-on (:decentralise :alexandria)
  :components ((:module example
                        :components ((:file "example")))))
