;;;; example.lisp: a short example for using cl-decentralised to create
;;;                a very simple synchronised data store.
;;; This file is a part of cl-decentralise.

(defpackage :decentralise-example
  (:use :cl :decentralise))

(in-package :decentralise-example)

(defclass my-system (system)
  ((data-table :initform (make-hash-table :test 'equal)
               :accessor data-table)
   (meta-table :initform (make-hash-table :test 'equal)
               :accessor meta-table)))

(defmethod listing-generator ((system my-system))
  (let ((alist (alexandria:hash-table-alist (meta-table system))))
    (lambda ()
      (if (null alist)
          nil
          (destructuring-bind (name version &rest channels)
              (pop alist)
            (declare (ignore channels))
            (values name version))))))

(defmethod put-item ((system my-system) name text version channels)
  (setf (gethash name (data-table system)) text
        (gethash name (meta-table system)) (cons version channels)))

(defmethod get-item ((system my-system) name)
  (multiple-value-bind (data win)
      (gethash name (data-table system))
    (if win
        (destructuring-bind (version &rest channels)
            (gethash name (meta-table system))
          (values data version channels))
        (error "not found"))))

(defvar *system* (make-instance 'my-system))
(bind *system*)
(accept *system* :log t)
