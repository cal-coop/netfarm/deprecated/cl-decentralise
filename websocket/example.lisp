;;;; example.lisp: a short example for using cl-decentralised to create
;;;                a very simple synchronised data store.
;;; This file is a part of cl-decentralise.

(defpackage :decentralise-websocket-example
  (:use :cl :decentralise :decentralise-websocket))

(in-package :decentralise-websocket-example)

(defclass my-system (websocket-listener)
  ((data-table :initform (make-hash-table :test 'equal)
               :accessor data-table)
   (meta-table :initform (make-hash-table :test 'equal)
               :accessor meta-table)))

(defmethod listing-generator ((system my-system))
  (let ((iterating t))
    (with-hash-table-iterator (iterator (meta-table system))
      (lambda ()
        (when iterating
          (multiple-value-bind (success name metadata)
              (iterator)
            (if success
                (values name (car metadata))
                (setf iterating nil))))))))

(defmethod put-item ((system my-system) name text version channels)
  (setf (gethash name (data-table system)) text
        (gethash name (meta-table system)) (cons version channels)))

(defmethod get-item ((system my-system) name)
  (multiple-value-bind (data win)
      (gethash name (data-table system))
    (if win
        (destructuring-bind (version &rest channels)
            (gethash name (meta-table system))
          (values data version channels))
        (error "not found"))))

(defvar *system* (make-instance 'my-system))
(defvar *server* (make-instance 'hunchensocket:websocket-acceptor :port 12345))

(push (lambda (request)
        (when (string= (hunchentoot:script-name request) "/decentralise.ws") *system*))
      hunchensocket:*websocket-dispatch-table*)
(hunchentoot:start *server*)
