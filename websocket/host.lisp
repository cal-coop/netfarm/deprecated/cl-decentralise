(in-package :decentralise-websocket)

(defclass websocket-listener (decentralise:system websocket-resource)
  ()
  (:default-initargs :client-class 'websocket-connection))

(defclass websocket-connection (decentralise:connection websocket-client)
  ((event-list :initform nil :accessor websocket-connection-event-list)
   (thread :initarg :thread :accessor websocket-connection-thread)))

;;; The hunchensocket part.

(defmethod client-connected ((listener websocket-listener) client)
  (setf (websocket-connection-thread client)
        (make-thread (lambda ()
                       (decentralise::repl client listener)))))

(defmethod client-disconnected ((listener websocket-listener) client)
  (setf (decentralise:system-connection-list listener)
        (delete client (decentralise:system-connection-list listener)
                :key #'decentralise::connection-meta-socket))
  (bt:destroy-thread (websocket-connection-thread client)))

(defmethod text-message-received (listener (client websocket-connection) text)
  (push (jsown:parse text) (websocket-connection-event-list client)))

;;; The cl-decentralise part.

(defmethod decentralise:bind ((system websocket-listener) &key host)
  (error "Websockets can't bind without a Hunchentoot listener, push me
into a dispatch table instead"))

(defmethod decentralise:read-event ((connection websocket-connection))
  (loop while (null (websocket-connection-event-list connection))
        do (sleep 0.05))
  (let* ((event (pop (websocket-connection-event-list connection)))
         (event-type (val event "type")))
    (macrolet ((prop (name) `(val event ,name)))
      (cond
        ((string= event-type "get") `(:get ,(prop "names")))
        ((string= event-type "block") `(:block ,(prop "name")
                                               ,(prop "version")
                                               ,(prop "channels")
                                               ,(prop "data")))
        ((string= event-type "ok") `(:ok ,(prop "name")))
        ((string= event-type "error") `(:error ,(prop "name")
                                               ,(prop "cause")))
        ((string= event-type "subscribe") `(:subscribe ,(prop "channels")))
        ((string= event-type "announce") `(:announce ,(string= (prop "new_state") "yes")))
        (t (error "invalid event-type: ~s" event-type))))))

(defmethod protocol-name ((connection websocket-connection)) "websocket")
