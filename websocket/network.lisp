(in-package :decentralise-websocket)

;;; Host

(defmethod decentralise:write-error ((connection websocket-connection) name &optional reason)
  (send-text-message connection
                     (to-json `(:obj ("type" . "error")
                                     ("name" . ,name)
                                     ("reason" . ,reason)))))

(defmethod decentralise:write-ok ((connection websocket-connection) name)
  (send-text-message connection
                     (to-json `(:obj ("type" . "ok")
                                     ("name" . ,name)))))

(defmethod decentralise:write-block ((connection websocket-connection) name values
                                      &key version channels)
  (let ((values
         (etypecase values
           (string values)
           (list (format nil "~{~a~^~%~}" values)))))
    (send-text-message connection
                       (to-json `(:obj ("type" . "block")
                                       ("name" . ,name)
                                       ("data" . ,values)
                                       ("version" . ,version)
                                       ("channels" . ,channels))))))

(defmethod decentralise:subscribe-channels ((connection websocket-connection) channels)
  (send-text-message connection
                     (to-json `(:obj ("type" . "subscribe")
                                     ("channels" . ,channels)))))

(defmethod decentralise:announce ((connection websocket-connection) &optional (announcep t))
  (send-text-message connection
                     (to-json `(:obj ("type" . "announce")
                                     ("new_state" . ,(if announcep "yes" "no"))))))

(defmethod decentralise:get-blocks ((connection websocket-connection) names)
  (send-text-message connection
                     (to-json `(:obj ("type" . "get")
                                     ("names" . ,names)))))
                                                 
(defmethod decentralise:close-connection ((connection websocket-connection))
  (hunchensocket:close-connection connection))

;;; Client

(defmethod decentralise:write-error ((connection websocket-client-connection) name &optional reason)
  (send-text-message (decentralise:connection-conn connection)
                     (to-json `(:obj ("type" . "error")
                                     ("name" . ,name)
                                     ("reason" . ,reason)))))

(defmethod decentralise:write-ok ((connection websocket-client-connection) name)
  (send-text-message (decentralise:connection-conn connection)
                     (to-json `(:obj ("type" . "ok")
                                     ("name" . ,name)))))

(defmethod decentralise:write-block ((connection websocket-client-connection) name values
                                      &key version channels)
  (let ((values
         (etypecase values
           (string values)
           (list (format nil "~{~a~^~%~}" values)))))
    (wsd:send (decentralise:connection-conn connection)
              (to-json `(:obj ("type" . "block")
                              ("name" . ,name)
                              ("data" . ,values)
                              ("version" . ,version)
                              ("channels" . ,channels))))))

(defmethod decentralise:subscribe-channels ((connection websocket-client-connection) channels)
  (wsd:send (decentralise:connection-conn connection)
            (to-json `(:obj ("type" . "subscribe")
                            ("channels" . ,channels)))))

(defmethod decentralise:announce ((connection websocket-client-connection) &optional (announcep t))
  (wsd:send (decentralise:connection-conn connection)
            (to-json `(:obj ("type" . "announce")
                            ("new_state" . ,(if announcep "yes" "no"))))))

(defmethod decentralise:get-blocks ((connection websocket-client-connection) names)
  (wsd:send (decentralise:connection-conn connection)
            (to-json `(:obj ("type" . "get")
                            ("names" . ,names)))))
                                                 
(defmethod decentralise:close-connection ((connection websocket-client-connection))
  (wsd:close-connection (decentralise:connection-conn connection)))
