(in-package :decentralise-websocket)

(defclass websocket-client-connection (decentralise:connection)
  ((decentralise::port :initarg :port :initform 80 :reader decentralise:connection-port)
   (https :initarg :https :initform nil :reader decentralise-websocket-client-https-p)
   (event-list :initform nil :accessor websocket-connection-event-list)))

(defmethod initialize-instance :after ((client websocket-client-connection) &key)
  (let ((connection (wsd:make-client
                     (format nil "~:[ws~;wss~]://~a:~d/decentralise.ws"
                             (decentralise-websocket-client-https-p client)
                             (decentralise:connection-ip client)
                             (decentralise:connection-port client)))))
    (setf (decentralise:connection-conn client) connection)
    (wsd:on :message connection
            (lambda (message)
              (push (jsown:parse message)
                    (websocket-connection-event-list client))))
    (wsd:start-connection connection)))

(defmethod decentralise:read-event ((connection websocket-client-connection))
  (loop while (null (websocket-connection-event-list connection))
        do (sleep 0.05))
  (let* ((event (pop (websocket-connection-event-list connection)))
         (event-type (val event "type")))
    (macrolet ((prop (name) `(val event ,name)))
      (cond
        ((string= event-type "get") `(:get ,(prop "names")))
        ((string= event-type "block") `(:block ,(prop "name")
                                               ,(prop "version")
                                               ,(prop "channels")
                                               ,(prop "data")))
        ((string= event-type "ok") `(:ok ,(prop "name")))
        ((string= event-type "error") `(:error ,(prop "name")
                                               ,(prop "reason")))
        ((string= event-type "subscribe") `(:subscribe ,(prop "channels")))
        ((string= event-type "announce") `(:announce ,(string= (prop "new_state") "yes")))
        (t (error "invalid event-type: ~s" event-type))))))

(defmethod protocol-name ((connection websocket-client-connection)) "websocket")

(setf (gethash "websocket" decentralise:*known-protocols*)
      (lambda (ip port timeout)
        (trivial-timeout:with-timeout (timeout)
          (make-instance 'websocket-client-connection :ip ip :port port))))
