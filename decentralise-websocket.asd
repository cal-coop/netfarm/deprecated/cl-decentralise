(defsystem decentralise-websocket
  :depends-on (:hunchentoot :hunchensocket :bordeaux-threads
               :jsown :decentralise :websocket-driver
               :trivial-timeout)
  :components ((:module websocket
                        :components ((:file "package")
                                     (:file "host")
                                     (:file "client")
                                     (:file "network")))))
