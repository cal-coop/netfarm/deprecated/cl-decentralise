(defpackage decentralise-tests
  (:use :cl :decentralise :fiveam)
  (:export #:run-tests
           #:gitlab-ci-test))
