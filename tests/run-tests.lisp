(in-package :decentralise-tests)

(defun run-tests ()
  (run! '(value-tests)))

(defun gitlab-ci-test ()
  (let ((fiveam:*on-error* :debug)
	(fiveam:*on-failure* :debug)
	#+foo (*debugger-hook* (lambda (c h)
			   (declare (ignore c h))
			   (uiop:quit -1))))
    (run-tests)))
