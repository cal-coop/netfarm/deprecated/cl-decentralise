(in-package :decentralise-tests)

(defun remove-trailing-space (string)
  (string-right-trim #.(coerce '(#\Space #\Newline) 'string)
                     string))

;; We need to emulate both worker threads synchronously.
;; So to do that, we:
;; feed in a string
;; run HANDLE-LINES-FROM until EOF
;; pop off all the objects to be written
;; get the string from OUTPUT-STREAM
(defmacro test-output (system input &optional expected-output)
  (alexandria:with-gensyms (input-stream output-stream two-way-stream
                            connection-meta connection)
    `(progn
       (let* (;; Streams
              (,input-stream (make-string-input-stream ,input))
              (,output-stream (make-string-output-stream))
              (,two-way-stream (make-two-way-stream ,input-stream ,output-stream))
              ;; Internal decentralise values.
              (,connection (make-instance 'decentralise::connection
                                          :ip "This doesn't matter!"
                                          :port 12345
                                          :connection ,two-way-stream))
              (,connection-meta (decentralise::make-connection-meta)))
         ;; Loop until there's nothing to read.
         (loop
            while (peek-char nil ,two-way-stream nil nil)
            do (decentralise::handle-lines-from ,connection
                                                ,system
                                                ,connection-meta))
         ;; Write every object out.
         (loop
            for value = (pop (decentralise::connection-meta-stack ,connection-meta))
            while value
            do (decentralise::write-object ,system ,connection value))
         ,(if expected-output
              `(string= (remove-trailing-space (get-output-stream-string ,output-stream))
                        ,expected-output)
              `(remove-trailing-space (get-output-stream-string ,output-stream)))))))
